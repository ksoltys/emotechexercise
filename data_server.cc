/*
 *
 * Copyright 2015, Google Inc.
 * All rights reserved.
 *
 * Modified by Karolina Soltys.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *     * Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above
 * copyright notice, this list of conditions and the following disclaimer
 * in the documentation and/or other materials provided with the
 * distribution.
 *     * Neither the name of Google Inc. nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#include <iostream>
#include <fstream>  
#include <memory>
#include <string>

#include <grpc++/grpc++.h>

#include "message.grpc.pb.h"

using grpc::Server;
using grpc::ServerBuilder;
using grpc::ServerContext;
using grpc::Status;
using dataexchange::DataRequest;
using dataexchange::DataReply;
using dataexchange::DataServer;
using dataexchange::UpdateNumberMessage;


DataReply currentData;

class DataServerServiceImpl final : public DataServer::Service {

  Status SendData(ServerContext* context, const DataRequest* request,
                  DataReply* reply) override {
    reply->set_number(currentData.number());
    reply->set_text(currentData.text());
    reply->set_image(currentData.image());
    return Status::OK;
  }

  Status UpdateNumber(ServerContext* context, const UpdateNumberMessage* update, DataReply* reply) override {
    currentData.set_number(update->number());
    return Status::OK;
  }

};

void RunServer() {
  std::string server_address("0.0.0.0:50051");
  DataServerServiceImpl service;
  ServerBuilder builder;
  // Listen on the given address without any authentication mechanism.
  builder.AddListeningPort(server_address, grpc::InsecureServerCredentials());
  // Register "service" as the instance through which we'll communicate with
  // clients. In this case it corresponds to an *synchronous* service.
  builder.RegisterService(&service);
  // Finally assemble the server.
  std::unique_ptr<Server> server(builder.BuildAndStart());
  std::cout << "Server listening on " << server_address << std::endl;

  // Wait for the server to shutdown. Note that some other thread must be
  // responsible for shutting down the server for this call to ever return.
  server->Wait();
}

void initialise(int number, std::string text, std::string image) {
    currentData.set_number(number);
    currentData.set_text(text);
    currentData.set_image(image);
  }

std::string readFileBytes(std::string filename) {  
    std::ifstream ifs(filename, std::ios::binary|std::ios::ate);
    std::ifstream::pos_type pos = ifs.tellg();

    std::vector<char> result(pos);

    ifs.seekg(0, std::ios::beg);
    ifs.read(&result[0], pos);
    std::string res(result.begin(),result.end());

    return res;
}  

int main(int argc, char** argv) {
  std::string image = readFileBytes("cat.jpg");
  initialise(0, "This is a text.", image);

  RunServer();

  return 0;
}
