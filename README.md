# README #

A prototype / proof of concept solution to the Emotech coding challenge: 
"Write a simple client/server app that uses the gRPC communication protocol, to exchange data. Language C++. Data shall be one number, one string and a jpeg image."

Based on the examples in the gRPC library. Written by Karolina Soltys.

# Features #
- The exchange can be bi-directional: the clients can read the current data from the server, but also update some data (the number field) on the server when run with a command line argument.

# Assumptions #
- No checked exceptions.
- The only field that can be updated by the clients is the number (otherwise we'd need to write a more complex control structure for the client, which isn't of much interest in a proof of concept solution). 

# Usage #
Server:

$ ./data_server

Client:

$ ./data_client   
(displays the current data values on the server; saves the file stored on the server to "out.jpg")

$ ./data_client [number]  
(updates the current number stored on the server to [number] displays the current data values on the server; saves the file stored on the server to "out.jpg")