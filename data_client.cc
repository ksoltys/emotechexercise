/*
 *
 * Copyright 2015, Google Inc.
 * All rights reserved.
 *
 * Modified by Karolina Soltys.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *     * Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above
 * copyright notice, this list of conditions and the following disclaimer
 * in the documentation and/or other materials provided with the
 * distribution.
 *     * Neither the name of Google Inc. nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#include <iostream>
#include <fstream>
#include <memory>
#include <string>

#include <grpc++/grpc++.h>

#include "message.grpc.pb.h"

using grpc::Channel;
using grpc::ClientContext;
using grpc::Status;
using dataexchange::DataRequest;
using dataexchange::DataReply;
using dataexchange::DataServer;
using dataexchange::UpdateNumberMessage;

class DataClient {
 public:
  DataClient(std::shared_ptr<Channel> channel)
      : stub_(DataServer::NewStub(channel)) {}

  // Assambles the client's payload, sends it and presents the response back
  // from the server.
  DataReply RequestData() {
    // Request we are sending to the server.
    DataRequest request;

    // Container for the data we expect from the server.
    DataReply reply;

    // Context for the client. It could be used to convey extra information to
    // the server and/or tweak certain RPC behaviors.
    ClientContext context;

    // The actual RPC.
    Status status = stub_->SendData(&context, request, &reply);

    // Act upon its status.
    if (status.ok()) {
      return reply;
    } else {
      std::cout << status.error_code() << ": " << status.error_message()
                << std::endl;
    }
  }

  DataReply UpdateNumber(int number) {
    UpdateNumberMessage update;
    DataReply reply;
    update.set_number(number);
    
    ClientContext context;

    Status status = stub_->UpdateNumber(&context, update, &reply);

    if (status.ok()) {
      return reply;
    } else {
      std::cout << status.error_code() << ": " << status.error_message()
                << std::endl;
    }
  }

 private:
  std::unique_ptr<DataServer::Stub> stub_;
};

int main(int argc, char** argv) {
  // Instantiate the client. It requires a channel, out of which the actual RPCs
  // are created. This channel models a connection to an endpoint (in this case,
  // localhost at port 50051). We indicate that the channel isn't authenticated
  // (use of InsecureChannelCredentials()).
  DataClient dataClient(grpc::CreateChannel(
      "localhost:50051", grpc::InsecureChannelCredentials()));

  if (argc > 1) {
    dataClient.UpdateNumber(atoi(argv[1]));
    std::cout << "Updated the current number to: " << argv[1] << std::endl;    
  }

  DataReply reply = dataClient.RequestData();
  std::cout << "Current number: " << reply.number() << std::endl;
  std::cout << "Current text: " << reply.text() << std::endl;

  std::ofstream outfile("out.jpg", std::ios::binary);
  outfile << reply.image();
  std::cout << "Current image saved to out.jpg." << std::endl;
  return 0;
}
